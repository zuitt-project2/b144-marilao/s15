// Operators

/*
	Add + = Sum
	Subtract - = Difference
	Multiply * = product
	Divide / = Quotient
	Modulus % =  Remainder


*/
function mod(){
	return 9 % 2;
}
console.log(mod());
// 8 * (6 + 2) - (2 * 3)
// 8 * 8 - 6


//Assignment operator (=)
/*
	+= , -=, *=, /=, %=
*/
	// let x = 1;

	let sum = 1;
	// sum = sum + 1;
	sum += 1;

	console.log(sum);

// Increment and decrement (++,--)
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;
// Pre- increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// Post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// Post-decrement
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Comparison Operators - returns boolean values

	// Equality Operator(==) - checks certain values if equal or not
// sample:

let juan = 'juan';
console.log(1 == 1);
console.log(0 == false);
console.log('juan' == juan);

// Strict Equality (===) - checks the type of the value

console.log(1 === true); // output is false.

// Inequality Operator (!=) - means type is not equal
console.log("Inequality Operator");
console.log(1 != 1);
console.log('Juan' != juan);
// Strict Inequality(!==) - means type and value is not equal
console.log(0 !== false); // output is true.

// Other comparison operators
/*
	> - greater than
	< - Less than
	>= - greater than or equal
	<= - less than or equal

*/
// Logical Operators

let isLegalAge = true;
let isRegistered = false;

/*
	And Operator (&&) - returns true if all operands are true.
	true true = true
	false true = false
	true false = false
	false false = false
	Or Operator (||) - returns true if at least one operands is true.
	true true = true
	false true = true
	true false = true
	false false = false
*/
// And Operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet); 

// Or operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR Operator: " + someRequirementsMet);

// Selection Control Structures
/* IF statement
	- execute a statement if a specified condition is true 
	Syntax
	if(condition){
		statement/s;
	}
*/
let num = -1;

if (num < 0){
	console.log('Hello'); 
} 

// Mini activity
let num1 = 10;

if(num1 >= 10){
	console.log('Welcome to Zuitt');
}
/* IF-ELSE statement
	- executes a statement if the previous condition returns false.
	Syntax:
	if(condition){
	statement/s;
	}
	else{
		statement/s;
	}
*/

num1 = 5;

if(num1 >= 10){
	console.log('Number is greater or equal to 10');
}
else{
	console.log('Number is not greater or equal to 10');
}

// Mini activity

/*age = parseInt(prompt("Please provide age: "));

if(age > 59){
	alert("Senior age");
}
else{
	alert("invalid age");
}*/

// IF-ELSEIF-ELSE statement
/* Syntax:
if(condition){
	statement/s;
}
else if(condition){
	statement/s;
}
else if(condition){
	statement/s;
}
.
.
.
else(condition){
	statement/s;
}

	1 - Quezon city
	2 - Valenzuela city
	3 - Pasig city
	4 - Taguig
*/

let city = parseInt(prompt("Enter a number: "));

if(city === 1){
	alert("Welcome to Quezon City");
}
else if(city === 2){
	alert("Welcome to Valenzuela City");
}
else if(city === 3){
	alert("Welcome to Pasig City");
}
else if(city === 4){
	alert("Welcome to Taguig City");
}
else{
	alert("Invalid Number");
}

let message = '';

function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return 'Not a typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'Tropical depression detected.';
	}
	else if(windSpeed >= 61 && windSpeed <= 88){
		return 'Tropical storm detected.';
	}
	else if(windSpeed >= 89 && windSpeed <= 117){
		return 'Severe tropical storm detected';
	}
	else{
		return 'Typhoon detected.';
	}
}
message = determineTyphoonIntensity(70);
console.log(message);

// Ternary operator - short version 
/* Syntax:
	(condition) ? iftrue : ifFalse
*/

/*let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);


let name;

function isOfLegalAge(){
	name = 'John';
	return 'You are of the legal age limit';
}
function isUnderAge(){
	name = 'jane';
	return 'You are under the age limit';
}
let age = parseInt(prompt("What is your age?"));
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert("Result of Ternary Operator in functions: " + legalAge + ', ' + name);*/

// Switch statement
/* Syntax:
	switch (expression){
		case value1:
			statement/s;
			break;
		case value2:
			statement/s;
			break;
			case valuen:
			break;
			default:
				statement/s;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase();
switch (day){
	case 'sunday':
		alert("The color of the day is red");
		break;
	case 'monday':
		alert("The color of the day is orange");
		break;
	case 'tuesday':
		alert("The color of the day is yellow");
		break;
	case 'wednesday':
		alert("The color of the day is green");
		break;
	case 'thursday':
		alert("The color of the day is blue");
		break;
	case 'friday':
		alert("The color of the day is indigo");
		break;
	case 'saturday':
		alert("The color of the day is violet");
		break;
		default:
		alert("Please input valid day");

}

// Try-Catch-Finally Statement - commonly used for error handling

function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch (error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.")
	}
}
showIntensityAlert(56);